<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%like}}`.
 */
class m200504_040015_create_like_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%like}}', [
            'id' => $this->primaryKey(),
            'post_id'=>$this->integer(11)->notNull(),
            'created_by'=>$this->integer(11)->notNull(),
            'created_at'=>$this->timestamp()->defaultExpression('CURRENT_TIMESTAMP')->notNull(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%like}}');
    }
}

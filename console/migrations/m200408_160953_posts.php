<?php

use yii\db\Migration;

/**
 * Class m200408_160953_posts
 */
class m200408_160953_posts extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('posts', [
            'id' => $this->primaryKey(),
            'post_by' => $this->integer(11)->notNull(),
            'rating' => $this->string(10)->notNull(),
            'category_id' => $this->string(11)->defaultValue(1)->notNull(),
            'title' => $this->string(100)->notNull(),
            'body' => $this->text()->notNull(),
            'image' => $this->string(200)->notNull(),
            'slug' => $this->string(255)->notNull(),
            'status' => $this->string(1)->defaultValue(1)->notNull(),
            'created_at' => $this->timestamp(100)->defaultExpression('CURRENT_TIMESTAMP')->notNull(),
        ]);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200408_160953_posts cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200408_160953_posts cannot be reverted.\n";

        return false;
    }
    */
}

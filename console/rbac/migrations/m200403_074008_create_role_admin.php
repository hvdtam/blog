<?php

use yii2mod\rbac\migrations\Migration;

class m200403_074008_create_role_admin extends Migration
{
    public function safeUp()
    {
        $this->createRole('admin', 'admin has all available permissions.');
        $this->createRole('user', 'user has CRUD permissions.');
        $this->createRole('guest', 'guest has only view permissions.');
    }

    public function safeDown()
    {
        echo "m200403_074008_create_role_admin cannot be reverted.\n";
        $this->removeRole('admin');
        return false;
    }
}
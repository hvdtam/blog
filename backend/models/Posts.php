<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "posts".
 *
 * @property int $id
 * @property int $post_by
 * @property int|null $category_id
 * @property string $title
 * @property string $body
 * @property string $image
 * @property string $slug
 * @property int $status
 * @property string $created_at
 *
 * @property User $postBy
 */
class Posts extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'posts';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['post_by', 'title', 'body', 'image'], 'required'],
            [['post_by', 'category_id', 'status'], 'integer'],
            [['body'], 'string'],
            [['created_at'], 'safe'],
            [['title'], 'string', 'max' => 100],
            [['image'], 'string', 'max' => 200],
            [['slug'], 'string', 'max' => 255],
//            [['post_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['post_by' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'post_by' => Yii::t('app', 'Post By'),
            'category_id' => Yii::t('app', 'Category ID'),
            'title' => Yii::t('app', 'Title'),
            'body' => Yii::t('app', 'Body'),
            'image' => Yii::t('app', 'Image'),
            'slug' => Yii::t('app', 'Slug'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created At'),
        ];
    }

    /**
     * Gets query for [[PostBy]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPostBy()
    {
        return $this->hasOne(User::className(), ['id' => 'post_by']);
    }
    public function getPostByCat($id){
        $data = Posts::find()->select('id')->where('id',$id)->asArray()->all();
    }
}

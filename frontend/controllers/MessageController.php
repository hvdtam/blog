<?php


namespace frontend\controllers;
use yii\web\Controller;
use frontend\models\Conversation;
use frontend\models\Message;
use bubasuma\simplechat\controllers\ControllerTrait;

class MessageController extends Controller
{
    use ControllerTrait;

    /**
     * @return string
     */
    public function getMessageClass()
    {
        return Message::className();
    }

    /**
     * @return string
     */
    public function getConversationClass()
    {
        return Conversation::className();
    }
}
<?php

namespace frontend\controllers;

use frontend\controllers\Post;
use frontend\models\Category;
use frontend\models\Posts;
use yii\data\ActiveDataProvider;
use yii\data\Pagination;
use yii\web\Controller;
use yii\filters\AccessControl;
use Yii;
use yii\web\UploadedFile;
use yii\web\HttpException;

class PostsController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['create', 'update'],
                'rules' => [
                    [
                        'actions' => ['create', 'update'],
                        'allow' => true,
                        'roles' => ['@']
                    ]
                ]
            ]
        ];
    }

    public function actionIndex()
    {
        $user_id = Yii::$app->user->identity->id;
        $model = Posts::find()->where(['post_by' => $user_id])->all();
        return $this->render('index', ['model' => $model]);
    }

    public function actionCategory($id)
    {
        $pages = new Pagination();
        $model = Posts::find()->where(['category_id' => $id])->limit(10)->all();
        return $this->render('category', ['model' => $model, 'pages' => $pages]);
    }

    public function actionCreate()
    {
        $model = new Posts();
        if ($model->load(Yii::$app->request->post())) {
            $model->post_by = Yii::$app->user->identity->getId();
            $images = UploadedFile::getInstances($model, 'image');
            foreach ($images as $image) {
                $image->saveAs('images/upload/' . $image->baseName . '.' . $image->extension);
            }
            $model->image = $image->baseName . '.' . $image->extension;
            if ($model->save(false)) {
                return $this->redirect(['post', 'id' => $model->id]);
            }
        }
        return $this->render('create', ['model' => $model]);
    }

    public function actionUpdate($id)
    {
        $user_id = Yii::$app->user->identity->id;
        $model = Posts::findOne(['id' => $id, 'post_by' => $user_id]);
        if ($model->load(Yii::$app->request->post())) {
            $model->post_by = Yii::$app->user->identity->getId();
            if ($model->save(false)) {
                return $this->redirect(['post', 'id' => $id]);
            }
        }
        return $this->render('update', ['model' => $model]);
    }

    public function actionDelete($id)
    {
        $user_id = Yii::$app->user->identity->id;
        if (Posts::findOne(['id' => $id, 'post_by' => $user_id])) {
            $model = Posts::find()->where(['id' => $id])->one();
            if ($model->delete()) {
                return $this->redirect(['index']);
            }
        }
    }

    public function actionPost($id)
    {
        $post = Posts::findOne(['id' => $id]);
        return $this->render('post', ['post' => $post]);
    }
}
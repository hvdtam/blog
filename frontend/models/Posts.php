<?php

namespace frontend\models;

use Yii;
use common\models\User;
use skeeks\yii2\slug\SlugBehavior;

/**
 * This is the model class for table "posts".
 *
 * @property int $id
 * @property int $post_by
 * @property string $title
 * @property string $body
 * @property string $image
 * @property int $status
 * @property string $slug
 * @property string $created_at
 *
 * @property User $postBy
 */
class Posts extends \yii\db\ActiveRecord
{
    public $captcha;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'posts';
    }

    public function behaviors()
    {
        return [
            'slug' => [
                'class' => 'skeeks\yii2\slug\SlugBehavior',
                'slugAttribute' => 'slug',                      //The attribute to be generated
                'attribute' => 'title',                          //The attribute from which will be generated
                // optional params
                'maxLength' => 64,                              //Maximum length of attribute slug
                'slugifyOptions' => [
                    'trim' => true,
                    'lowercase' => true,
                    'separator' => '-',
                ],
            ],
            'image' => [
                'class' => 'rico\yii2images\behaviors\ImageBehave',
            ],
        ];
    }

    /**
     * {@inheritdoc}f
     */
    public function rules()
    {
        return [
            [['title', 'image', 'body', 'captcha', 'slug'], 'required'],
            [['body'], 'string'],
            [['id', 'post_by', 'created_at', 'rating'], 'safe'],
            [['category_id'], 'exist', 'skipOnError' => true, 'targetClass' => Category::className(), 'targetAttribute' => ['category_id' => 'id']],
            [['image'], 'string', 'max' => 200],
            [['slug'], 'string', 'max' => 255],
            [['image'], 'file', 'extensions' => 'jpg,png,gif', 'maxFiles' => 3],
            [['post_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['post_by' => 'id']],
            ['captcha', 'captcha'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'post_by' => Yii::t('app', 'Post By'),
            'rating' => Yii::t('app', 'Rating'),
            'category_id' => Yii::t('app', 'Category ID'),
            'title' => Yii::t('app', 'Title'),
            'body' => Yii::t('app', 'Body'),
            'image' => Yii::t('app', 'Image'),
            'status' => Yii::t('app', 'Status'),
            'slug' => Yii::t('app', 'Slug'),
            'created_at' => Yii::t('app', 'Created At'),
        ];
    }

    /**
     * Gets query for [[PostBy]].
     *
     * @return \yii\db\ActiveQuery
     */

    public function getPostby()
    {
        return $this->hasOne(User::className(), ['id' => 'post_by']);
    }

    public function getCategoryid()
    {
        return $this->hasOne(Category::className(), ['id' => 'category_id']);
    }
}
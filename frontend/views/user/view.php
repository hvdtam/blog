<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\helpers\Url;
use frontend\components\SmallBody;

/* @var $this yii\web\View */
/* @var $model app\models\User */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Users'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<section class="section-content">
    <div class="container">
        <div class="row ">
            <h1>User: <?= Html::encode($this->title) ?></h1>
            <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                    'name',
                    'phone',
                    'username',
                    'email:email',
                ],
            ]) ?>
            <div class="col-md-1 full-width-content">
                <?php foreach ($category as $item) { ?>
                    <article class="content-item">
                        <div class="entry-media">
                            <div class="post-title">
                                <h2>
                                    <a href="<?= Url::toRoute(['/posts/post', 'id' => $item->id]) ?>"><?= $item->title ?></a>
                                </h2>
                                <div class="entry-date">
                                    <ul>
                                        <li>
                                            <a href="<?= Url::toRoute(['/posts/category/', 'id' => $item->categoryid->id]) ?>"><?= $item->categoryid->title ?></a>
                                        </li>
                                        <li>
                                            <a href="<?= Url::toRoute(['/user/view/', 'id' => $item->postby->id]) ?>"><?= $item->postby->name ?></a>
                                        </li>
                                        <li><?= $item->created_at ?></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="bubble-line"></div>
                            <div class="post-content full-width">
                                <img src="/images/upload/<?= $item->image ?>">
                                <p>
                                    <?= SmallBody::widget(['body' => $item->body]) ?>
                                </p>
                            </div>
                            <div class="bubble-line"></div>
                            <div class="post-footer">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <a href="<?= Url::toRoute(['/posts/post', 'id' => $item->id]) ?>"
                                           class="button">Continue reading</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </article>
                <?php } ?>
            </div>
        </div>
    </div>
</section>

<?php

use frontend\models\Posts;
use yii\helpers\Html;
use yii\helpers\Url;
use yii2mod\comments\widgets\Comment;
use yii\helpers\StringHelper;

$this->title = $post->title;
?>
<?= $this->registerMetaTag([

])
?>
<section class="section-content">
    <div class="container">
        <div class="row">
            <div class="col-sm-8 col-md-8">
                <article class="content-item">
                    <div class="entry-media">
                        <div class="post-title">
                            <h2><a href="javascript:;"><?=$post->title?></a></h2>
                            <div class="entry-date">
                                <ul>
                                    <li><?=$post->created_at?></li>
                                    <li>
                                        <?= \chiliec\vote\widgets\Vote::widget([
                                            'model' => $post,
                                            // optional fields
                                            'showAggregateRating' => false,
                                        ]);?>
                                    </li>
                                </ul>
                            </div>
                                    <a href="/index?r=posts%2Fupdate&id=<?= $post->id ?>"
                                       class="btn btn-primary btn-sm styled">Update</a>
                                    <a href="/index?r=posts%2Fdelete&id=<?= $post->id ?>"
                                       class="btn btn-success btn-sm styled"">Delete</a>
                        </div>
                        <div class="bubble-line"></div>
                        <div class="post-content">
                            <img src="/images/upload/<?=$post->image?>">
                            <?=$post->body?>
                        </div>
                        <div class="bubble-line"></div>
                        <div class="post-footer">
                            <div class="row">
                                <div class="col-sm-7 text-right">
                                    <div class="content-social">
                                        <a href="https://www.facebook.com/sharer.php?u=<?=Yii::$app->request->url?>"><i
                                                class="fa fa-facebook"></i><span>Facebook</span></a>
                                        <a href="https://twitter.com/intent/tweet?text=<?=Yii::$app->request->url?>"><i class="fa fa-twitter"></i><span>Twitter</span></a>
                                        <a href="https://www.linkedin.com/shareArticle?mini=true&url=<?=Yii::$app->request->url?>"><i
                                                class="fa fa-linkedin"></i><span>LinkedIn</span></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </article>

                <article class="content-item">
                    <div class="entry-media">
                        <div class="post-title">
                            <h2>Leave a reply</h2>
                        </div>
                        <div class="bubble-line"></div>
                        <div class="post-content comment">
                            <?php
                            $model = Posts::find()->where(['title'=>$post->title])->one();
                            echo Comment::widget([
                                'model' => $model,
                                'dataProviderConfig' => [
                                    'pagination' => [
                                        'pageSize' => 10
                                    ],
                                ]
                            ]); ?>
                        </div>
                    </div>
                </article>
            </div>
        </div>
    </div>
</section>

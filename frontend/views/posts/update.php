<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\file\FileInput;
use dosamigos\tinymce\TinyMce;
use frontend\models\Category;
use yii\helpers\ArrayHelper;

?>
<div class="container">
    <h3>Update</h3>
    <?php $form = ActiveForm::begin() ?>
    <?= $form->field($model, 'title')->textInput() ?>
    <?= $form->field($model, 'category_id')->dropDownList(
        ArrayHelper::map(Category::find()->all(), 'id', 'title'),
        ['prompt' => 'Select Category'])
    ?>
    <?= $form->field($model, 'body')->widget(TinyMce::className(), [
        'options' => ['rows' => 5],
        'language' => 'vi_VN',
        'clientOptions' => [
            'toolbar' => "undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent | link image"
        ]
    ]); ?>

    <!--    <? //= $form->field($model, 'image[]')->fileInput(['multiple'=>true,'accept'=>'image/*']); ?>-->
    <?= FileInput::widget([
        'model' => $model,
        'attribute' => 'image',
        'options' => ['multiple' => true]
    ]);
    ?>
    <?= $form->field($model, 'status')->dropDownList([1 => 'Active', 0 => 'Inactive']) ?>
    <?= Html::submitButton('Post', ['class' => 'btn btn-primary']) ?>
    <?php ActiveForm::end() ?>
</div>

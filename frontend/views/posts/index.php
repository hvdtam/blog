<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

?>

<div class="container">
    <h2><?= Yii::t('app', 'Manage your post') ?></h2>
    <a href="<?= Url::to(['create']) ?>" class="btn btn-success pull-right">Create Post</a>
    <table class="table table-striped table-bordered">
        <tr>
            <th>#</th>
            <th><?= Yii::t('app', 'Title') ?></th>
            <th><?= Yii::t('app', 'Posted Date') ?></th>
            <th></th>
        </tr>
        <?php
        $n = 1;
        foreach ($model as $post) {
            ?>
            <tr>
                <td><?= $n ?></td>
                <td><?= $post->title ?></td>
                <td><?= $post->created_at ?></td>
                <td><a href="<?= Url::to(['view', 'id' => $post->id]) ?>"><?= Yii::t('app', 'View') ?></a> |
                    <a href="<?= Url::to(['update', 'id' => $post->id]) ?>"><?= Yii::t('app', 'Update') ?></a> |
                    <a href="<?= Url::to(['delete', 'id' => $post->id]) ?>"><?= Yii::t('app', 'Delete') ?></a>
                </td>
            </tr>
            <?php
            $n++;
        } ?>
    </table>
</div>
<?php
?>
<div class="vote-row text-center" id="vote-<?=$modelId?>-<?=$targetId?>" data-placement="top" data-container="body" data-toggle="popover">
    <span  class="glyphicon glyphicon-chevron-up" onclick="vote(<?=$modelId?>, <?=$targetId?>, 'like'); return false;" style="cursor: pointer;"></span><br /><span id="vote-up-<?=$modelId?>-<?=$targetId?>"><?=$likes?></span>
    <div id="vote-response-<?=$modelId?>-<?=$targetId?>">
        <?php if ($showAggregateRating) { ?>
            <?=Yii::t('vote', 'Aggregate rating')?>: <?=$rating?>
        <?php } ?>
    </div>
</div>
<div itemprop="aggregateRating" itemscope itemtype="http://schema.org/AggregateRating">
    <meta itemprop="interactionCount" content="UserLikes:<?=$likes?>"/>
    <meta itemprop="interactionCount" content="UserDislikes:<?=$dislikes?>"/>
    <meta itemprop="ratingValue" content="<?=$rating?>"/>
    <meta itemprop="ratingCount" content="<?=$likes+$dislikes?>"/>
    <meta itemprop="bestRating" content="10"/>
    <meta itemprop="worstRating" content="0"/>
</div>
f
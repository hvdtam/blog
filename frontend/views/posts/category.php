<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use frontend\models\Posts;
use frontend\components\SmallBody;
use yii\widgets\LinkPager;

?>

<div class="container">

    <section class="section-content">
        <div class="container">
            <div class="row ">
                <div class="col-md-1 full-width-content">
                    <?php foreach ($model as $category) { ?>
                        <article class="content-item">
                            <div class="entry-media">
                                <div class="post-title">
                                    <h2>
                                        <a href="<?= Url::toRoute(['/posts/post', 'id' => $category->id]) ?>"><?= $category->title ?></a>
                                    </h2>
                                    <div class="entry-date">
                                        <ul>
                                            <li>
                                                <a href="<?= Url::toRoute(['/category/', 'id' => $category->categoryid->id]) ?>"><?= $category->categoryid->title ?></a>
                                            </li>
                                            <li>
                                                <a href="<?= Url::toRoute(['/user/view/', 'id' => $category->postby->id]) ?>"><?= $category->postby->name ?></a>
                                            </li>
                                            <li><?= $category->created_at ?></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="bubble-line"></div>
                                <div class="post-content full-width">
                                    <img src="/images/upload/<?= $category->image ?>">
                                    <p>
                                        <?= SmallBody::widget(['body' => $category->body]) ?>
                                    </p>
                                </div>
                                <div class="bubble-line"></div>
                                <div class="post-footer">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <a href="<?= Url::toRoute(['/posts/post', 'id' => $category->id]) ?>"
                                               class="button">Continue reading</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </article>
                    <?php } ?>
                </div>
            </div>
    </section>
</div>
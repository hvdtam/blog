<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\file\FileInput;
use yii\helpers\ArrayHelper;
use frontend\models\Category;
use yii\captcha\Captcha;

?>
<div class="container">
    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]) ?>
    <?= $form->field($model, 'title')->textInput() ?>

    <?= $form->field($model, 'category_id')->dropDownList(
        ArrayHelper::map(Category::find()->all(), 'id', 'title'),
        ['prompt' => 'Select Category'])
    ?>

    <?= \yii\redactor\widgets\Redactor::widget([
        'model' => $model,
        'attribute' => 'body'
    ]) ?>
    <?= Yii::t('app', 'Image') ?>
    <?= FileInput::widget([
        'model' => $model,
        'attribute' => 'image',
        'options' => ['multiple' => false,
            'required' => true]
    ]);
    ?>
    <?= $form->field($model, 'captcha')->widget(Captcha::className()) ?>
    <?= $form->field($model, 'status')->dropDownList([1 => 'Active', 0 => 'Inactive']) ?>
    <?= Html::submitButton('Post', ['class' => 'btn btn-primary']) ?>
    <?php ActiveForm::end() ?>
</div>
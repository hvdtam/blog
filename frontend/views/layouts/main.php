<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\helpers\Url;
use frontend\assets\AppAsset;
use frontend\components\LanguageDropdown;
use yii\bootstrap\NavBar;
use yii\bootstrap\Nav;
AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<body>

<div class="wrapper">
    <header id="header">
        <div class="logo" data-bg-image="images/bg-header.jpg">
            <h1>
                <a href="<?=Url::to('/site/index')?>">tamk Website</a>
            </h1>
        </div>

        <div class="menu-container">
            <div class="container">
                <div class="row">
                    <div  class="col-md-pull-5">
                        <nav class="main-nav">
                            <ul>

                                <li class=" current-menu-item menu-item-has-children">
                                    <a href="<?=Url::to(['index'])?>"><?=Yii::t('app','Home')?></a>
                                </li>
                                <li><?php
                                    $menuItems = [
                                        ['label' => LanguageDropdown::label(Yii::$app->language),
                                            'items' => LanguageDropdown::widget()]
                                    ];
                                    echo Nav::widget([
                                        'items' => $menuItems,
                                    ]);
                                    ?></li>
                                <?php if(Yii::$app->user->isGuest){ ?>
                                <li><a href="<?=Url::to(['/site/login'])?>"><?=Yii::t('app','Login')?></a></li>
                                <li><a href="<?=Url::to(['/site/signup'])?>"><?=Yii::t('app','Sign up')?></a></li>
                                <?php }else{?>

                                    <li>
                                        <div class="dropdown">
                                            <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown"><?= Yii::$app->user->identity->username ?>
                                                <span class="caret"></span></button>
                                            <ul class="dropdown-menu">
                                                <li><a href="<?=Url::to(['/posts'])?>"><?=Yii::t('app','Manage Your Post')?></a></li>
                                                <li><a href="/user/update?id=<?=Yii::$app->user->identity->getId()?>"><?=Yii::t('app','Edit Infomation')?></a></li>
                                                <li><?=
                                                    Html::a(
                                                        'Sign out', ['/site/logout'], ['data-method' => 'post']
                                                    )
                                                    ?></li>
                                            </ul>
                                        </div>
                                    </li>
                                <?php } ?>
                            </ul>
                            <a href="javascript:;" id="close-menu"> <i class="fa fa-close"></i></a>
                        </nav>
                    </div>
                    <div class="col-md-5 h-search">
                        <form class="search_form">
                            <input type="text" name="2" placeholder="Search and hit enter...">
                            <button type="submit"><i class="fa fa-search"></i></button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <?= $content ?>
    <footer id="footer">
        <div class="sub-footer">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="widget">
                        </div>
                        <div class="widget footer-cp-text">
                            <p>
                                &copy; <?= Html::encode(Yii::$app->name) ?> <?= date('Y') ?>.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>

    <script type="text/template" id="tpl-bubble-left">
        <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 15 30" enable-background="new 0 0 15 30" xml:space="preserve">
            <path fill-rule="evenodd" clip-rule="evenodd" fill="none" stroke="#000000" stroke-miterlimit="10" d="M0,29.4c0,0,7.5,0,7.5-7c0,0,7,0,7-7c0-0.1,0-0.1,0-0.2c0-0.1,0-0.1,0-0.2c0-7-7-7-7-7c0-7-7.5-7-7.5-7"/>
        </svg>
    </script>

    <script type="text/template" id="tpl-bubble-right">
        <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 15 30" enable-background="new 0 0 15 30" xml:space="preserve">
            <path fill-rule="evenodd" clip-rule="evenodd" fill="none" stroke="#000000" stroke-miterlimit="10" d="M15,29.4c0,0-7.5,0-7.5-7c0,0-7,0-7-7c0-0.1,0-0.1,0-0.2c0-0.1,0-0.1,0-0.2c0-7,7-7,7-7c0-7,7.5-7,7.5-7"/>
        </svg>
    </script>


    <!-- Include jQuery and Scripts -->
    <script type="text/javascript" src="/js/jquery.min.js"></script>
    <script type="text/javascript" src="/vendors/jquery.waypoints.min.js"></script>
    <script type="text/javascript" src="/vendors/isotope.pkgd.min.js"></script>
    <!-- Swiper -->
    <script type="text/javascript" src="/vendors/swiper/js/swiper.min.js"></script>
    <!-- Magnific-popup -->
    <script type="text/javascript" src="/js/scripts.js"></script>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>

<?php

/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\helpers\Url;
use frontend\components\SmallBody;
use yii\widgets\LinkPager;
?>


<hr>
<section class="section-content">
    <div class="container">
        <div class="row ">
            <div class="col-md-1 full-width-content">
                <?php foreach($posts as $post){?>
                <article class="content-item">
                    <div class="entry-media">
                        <div class="post-title">
                            <h2><a href="<?=Url::toRoute(['/posts/post','id'=>$post->id])?>"><?=$post->title?></a></h2>
                            <div class="entry-date">
                                <ul>
                                    <li><a href="<?=Url::toRoute(['/posts/category/','id'=>$post->categoryid->id]) ?>"><?=$post->categoryid->title?></a></li>
                                    <li><a href="<?=Url::toRoute(['/user/view/','id'=>$post->postby->id]) ?>"><?=$post->postby->name?></a></li>
                                    <li><?=$post->created_at?></li>
                                </ul>
                            </div>
                        </div>
                        <div class="bubble-line"></div>
                        <div class="post-content full-width">
                            <img src="/images/upload/<?=$post->image?>"><p>
                                <?=SmallBody::widget(['body'=>$post->body]) ?>
                            </p>
                        </div>
                        <div class="bubble-line"></div>
                        <div class="post-footer">
                            <div class="row">
                                <div class="col-sm-6">
                                    <a href="<?=Url::toRoute(['/posts/post','id'=>$post->id])?>" class="button">Continue reading</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </article>
                <?php } ?>
                <div class="post-navigation">
                    <?php
                    echo LinkPager::widget([
                        'pagination' => $pages,
                        'options'=>[
                            'class' => '',
                        ]
                    ]);
                    ?>
                </div>
        </div>
    </div>
</section>
</div>

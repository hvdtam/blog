<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/site.css',
        'vendors/bootstrap/css/bootstrap.min.css',
	    'vendors/bootstrap/css/bootstrap-theme.min.css',
	    'vendors/font-awesome/css/font-awesome.min.css',
	    'vendors/font-awesome/css/font-awesome.min.css',
	    'vendors/swiper/css/swiper.min.css',
	    'style.css',
	    'css/welcome.css',
        'https://fonts.googleapis.com/css?family=Merriweather:400,300,300italic,400italic,700,700italic,900,900italic%7CPlayball%7CMontserrat:400,700'
    ];
    public $cssOptions = [
    'type'=>'text/css'
    ];
    public $js = [
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
